/**
 * `NeoXConnectAPIError` error.
 *
 */
function NeoXConnectAPIError(message, code) {
  Error.call(this);
  Error.captureStackTrace(this, arguments.callee);
  this.name = 'NeoXConnectAPIError';
  this.message = message;
  this.code = code;
}

// Inherit from `Error`.
NeoXConnectAPIError.prototype.__proto__ = Error.prototype;


// Expose constructor.
module.exports = NeoXConnectAPIError;
