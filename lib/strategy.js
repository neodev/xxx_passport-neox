// Load modules.
var util = require('util')
  , OAuth2Strategy = require('passport-oauth2')
  , Profile = require('./profile')
  , InternalOAuthError = require('passport-oauth2').InternalOAuthError
  , NeoXConnectAPIError = require('./errors/neoxconnectapierror');


/**
 * `Strategy` constructor.
 *
 */
function Strategy(options, verify) {
  options = options || {};
  var server = options.server || 'https://portal.neoarea.com';
  options.authorizationURL = server + '/oauth/authorize';
  options.tokenURL = server + '/oauth/token';
  
  OAuth2Strategy.call(this, options, verify);
  this.name = 'neox';
  this._userProfileURL = server + '/api/profile';;
}

// Inherit from `OAuth2Strategy`.
util.inherits(Strategy, OAuth2Strategy);

/**
 * Retrieve user profile from NeoX.
 *
 * This function constructs a normalized profile, with the following properties:
 *
 *   - `provider`         always set to `NeoX`
 *   - `id`               the user's NeoX ID
 *   - `displayName`      the user's full name
 *
 * @param {string} accessToken
 * @param {function} done
 * @access protected
 */
Strategy.prototype.userProfile = function(accessToken, done) {
  this._oauth2.get(this._userProfileURL, accessToken, function (err, body, res) {
    var json;
    
    if (err) {
      if (err.data) {
        try {
          json = JSON.parse(err.data);
        } catch (_) {}
      }
        
      if (json && json.error) {
        return done(new NeoXConnectAPIError(json.error.message, json.error.code));
      }
      return done(new InternalOAuthError('Failed to fetch user profile', err));
    }
    
    try {
      json = JSON.parse(body);
    } catch (ex) {
      return done(new Error('Failed to parse user profile'));
    }
    
    var profile = Profile.parse(json);
    profile.provider  = 'neox';
    profile._raw = body;
    profile._json = json;
    
    done(null, profile);
  });
};


// Expose constructor.
module.exports = Strategy;
